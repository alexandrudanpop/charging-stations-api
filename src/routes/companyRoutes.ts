import { Router } from "express";
import { CompanyController } from "../controllers/CompanyController";

const router = Router();
const controller = new CompanyController();

router.get("/companies", controller.getAll);
router.get("/companies/:id", controller.getById);
router.post("/companies", controller.create);
router.put("/companies/:id", controller.update);
router.delete("/companies/:id", controller.delete);

export default router;
