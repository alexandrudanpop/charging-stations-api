import { Router } from "express";
import { StationController } from "../controllers/StationController";

const router = Router();
const controller = new StationController();

router.get("/stations", controller.getAll);
router.get("/stations/:id", controller.getById);
router.get("/stations/location/:latitude/:longitude", controller.getByLocation);
router.post("/stations", controller.create);
router.put("/stations/:id", controller.update);
router.delete("/stations/:id", controller.delete);

export default router;
