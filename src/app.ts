import express, { NextFunction } from "express";
import { Request, Response } from "express";
import companyRoutes from "./routes/companyRoutes";
import stationRoutes from "./routes/stationRoutes";

import swaggerUi = require("swagger-ui-express");

import apiSpecs from "./openApi.json";

const app = express();
app.use(express.json());

app.use(express.json());

app.use("/api", companyRoutes);
app.use("/api", stationRoutes);

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(apiSpecs));

app.use(
  (
    err: { stack: string; message?: string },
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    console.error(err.stack);
    res?.status(500)?.json({ message: err.message || "Internal Server Error" });
  },
);

export default app;
