import { NextFunction, Request, Response } from "express";
import { Company } from "../entitiy/Company";
import { dataSource } from "../app-data-source";

export class CompanyController {
  async getAll(req: Request, res: Response) {
    const page = parseInt(req.query.page as string, 10) || 1;
    const pageSize = parseInt(req.query.pageSize as string, 10) || 10;

    const offset = (page - 1) * pageSize;

    const result = await dataSource.getRepository(Company).findAndCount({
      relations: {
        parentCompany: true,
      },
      take: pageSize,
      skip: offset,
      order: {
        id: "ASC",
      },
    });

    return res.json({ rows: result[0], count: result[1] });
  }

  async getById(req: Request, res: Response) {
    if (!req.params.id) {
      return res.status(400).json({ message: "Missing resource id" });
    }

    const company = await dataSource.getRepository(Company).findOne({
      where: { id: Number(req.params.id) },
      relations: {
        stations: true,
        parentCompany: true,
      },
    });

    if (!company) {
      return res.status(404).json({ message: "Company not found" });
    }

    res.json(company);
  }

  // to fix -> right now we can create companies with the same name
  async create(req: Request, res: Response, next: NextFunction) {
    const { name, parentCompany } = req.body;

    if (!name) {
      return res.status(400).json({ message: "Missing company name" });
    }

    try {
      const company = dataSource
        .getRepository(Company)
        .create({ name, parentCompany });
      const savedCompany = await dataSource
        .getRepository(Company)
        .save(company);

      return res.status(201).json(savedCompany);
    } catch (err) {
      next(err);
    }
  }

  async update(req: Request, res: Response, next: NextFunction) {
    if (!req.params.id) {
      return res.status(400).json({ message: "Missing resource id" });
    }

    const { name, parentCompany } = req.body;

    const company = await dataSource
      .getRepository(Company)
      .findOne({ where: { id: Number(req.params.id) } });

    if (!company) {
      return res.status(404).json({ message: "Company not found" });
    }

    try {
      dataSource
        .getRepository(Company)
        .merge(company, { name, parentCompany: parentCompany });
      const updatedCompany = await dataSource
        .getRepository(Company)
        .save(company);

      return res.json(updatedCompany);
    } catch (err) {
      next(err);
    }
  }

  async delete(req: Request, res: Response) {
    await dataSource.getRepository(Company).delete(req.params.id);

    return res.json({ message: "Company deleted successfully" });
  }
}
