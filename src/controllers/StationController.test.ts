import { Request, Response } from "express";
import { StationController } from "./StationController";
import { dataSource } from "../app-data-source";

jest.mock("../app-data-source", () => ({
  dataSource: {
    getRepository: jest.fn(),
  },
  initializeDataSource: jest.fn(),
}));

describe("StationController", () => {
  let mockResponse: Partial<Response>;

  beforeEach(() => {
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("get all", () => {
    it("should get all stations", async () => {
      const mockStations = [
        [
          { id: 1, name: "Station 1" },
          { id: 2, name: "Station 2" },
        ],
        2,
      ];
      dataSource.getRepository = jest.fn().mockReturnValue({
        findAndCount: jest.fn().mockResolvedValue(mockStations),
      });

      const mockRequest = { query: { page: 1, pageSize: 2 } } as any as Request;
      await StationController.prototype.getAll(
        mockRequest,
        mockResponse as Response,
      );

      expect(mockResponse.json).toHaveBeenCalledWith({
        rows: mockStations[0],
        count: mockStations[1],
      });
    });
  });

  describe("get by id", () => {
    it("when no station returns 404", async () => {
      dataSource.getRepository = jest.fn().mockReturnValue({
        findOne: jest.fn().mockResolvedValue(undefined),
      });

      const mockRequest = { params: { id: "1" } } as Request<any>;
      await StationController.prototype.getById(
        mockRequest,
        mockResponse as Response,
      );

      expect(mockResponse.status).toHaveBeenCalledWith(404);
    });

    it("should get a station by id", async () => {
      const mockStation = { id: 1, name: "Station 1" };
      dataSource.getRepository = jest.fn().mockReturnValue({
        findOne: jest.fn().mockResolvedValue(mockStation),
      });

      const mockRequest = { params: { id: "1" } } as Request<any>;
      await StationController.prototype.getById(
        mockRequest,
        mockResponse as Response,
      );

      expect(mockResponse.json).toHaveBeenCalledWith(mockStation);
    });
  });

  describe("create", () => {
    it("should handle invalid input", async () => {
      const mockStation = { name: "New Station" };
      dataSource.getRepository = jest.fn().mockReturnValue({
        create: jest.fn().mockReturnValue(mockStation),
        save: jest.fn().mockResolvedValue(mockStation),
      });

      const mockRequest = { body: {} } as Request;
      await StationController.prototype.create(
        mockRequest,
        mockResponse as Response,
        jest.fn(),
      );

      expect(mockResponse.status).toHaveBeenCalledWith(400);
      expect(mockResponse.json).toHaveBeenCalledWith({
        message: "Missing mandatory fields",
      });
    });

    it("should create a station", async () => {
      const mockStation = {
        name: "New Station",
        latitude: 1,
        longitude: 1,
        address: "Str Test",
        company: 1,
      };

      dataSource.getRepository = jest.fn().mockReturnValue({
        create: jest.fn().mockReturnValue(mockStation),
        save: jest.fn().mockResolvedValue(mockStation),
      });

      const mockRequest = { body: mockStation } as Request;
      await StationController.prototype.create(
        mockRequest,
        mockResponse as Response,
        jest.fn(),
      );

      expect(mockResponse.status).toHaveBeenCalledWith(201);
      expect(mockResponse.json).toHaveBeenCalledWith(mockStation);
    });
  });

  describe("update", () => {
    it("should update a station", async () => {
      const mockStation = { id: 1, name: "Station 1" };
      const mockUpdatedStation = {
        ...mockStation,
        name: "Updated Station",
      };
      dataSource.getRepository = jest.fn().mockReturnValue({
        findOne: jest.fn().mockResolvedValue(mockStation),
        merge: jest.fn().mockReturnThis(),
        save: jest.fn().mockResolvedValue(mockUpdatedStation),
      });

      const mockRequest = {
        params: { id: "1" },
        body: { name: "Updated Station" },
      } as Request<any>;

      await StationController.prototype.update(
        mockRequest,
        mockResponse as Response,
        jest.fn(),
      );

      expect(mockResponse.json).toHaveBeenCalledWith(mockUpdatedStation);
    });
  });

  describe("delete", () => {
    it("should delete a station", async () => {
      dataSource.getRepository = jest.fn().mockReturnValue({
        delete: jest.fn().mockResolvedValue({ affected: 1 }),
      });

      const mockRequest = { params: { id: "1" } } as Request<any>;
      await StationController.prototype.delete(
        mockRequest,
        mockResponse as Response,
      );

      expect(mockResponse.json).toHaveBeenCalledWith({
        message: "Station deleted successfully",
      });
    });
  });

  describe("getByLocation", () => {
    it("should handle stations not found", async () => {
      const req = {
        params: {
          latitude: "40.7128",
          longitude: "-74.0060",
          range: "1000",
        },
      } as any as Request;

      const next = jest.fn();

      dataSource.getRepository = jest.fn().mockReturnValue({
        createQueryBuilder: jest.fn().mockReturnThis(),
        select: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        setParameters: jest.fn().mockReturnThis(),
        getRawMany: jest.fn().mockResolvedValue([]),
      });

      await StationController.prototype.getByLocation(
        req,
        mockResponse as Response,
        next,
      );

      expect(mockResponse.status).toHaveBeenCalledWith(404);
      expect(mockResponse.json).toHaveBeenCalledWith({
        message: "Stations not found",
      });
    });

    it.each([
      {},
      {
        longitude: "-74.0060",
        range: "1000",
      },
      {
        longitude: "-74.0060",
      },
    ])("should handle missing latitude and/or longitude", async (params) => {
      const req = {
        params,
      } as any as Request;

      const next = jest.fn();

      dataSource.getRepository = jest.fn().mockReturnValue({
        createQueryBuilder: jest.fn().mockReturnThis(),
        select: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        setParameters: jest.fn().mockReturnThis(),
        getRawMany: jest.fn().mockResolvedValue([]),
      });

      await StationController.prototype.getByLocation(
        req,
        mockResponse as Response,
        next,
      );

      expect(mockResponse.status).toHaveBeenCalledWith(400);
      expect(mockResponse.json).toHaveBeenCalledWith({
        message: "Missing latitude and/or longitude",
      });
    });

    it("should return stations within range", async () => {
      const req = {
        params: {
          latitude: "40.7128",
          longitude: "-74.0060",
          range: "1000",
        },
      } as any as Request;

      const next = jest.fn();

      const mockStationData = [
        {
          id: 1,
          name: "Station A",
          address: "123 Main St",
          latitude: "40.1",
          longitude: "-74.1",
          company: "Company A",
          distance: 0.5, // Distance in kilometers
        },
      ];

      dataSource.getRepository = jest.fn().mockReturnValue({
        createQueryBuilder: jest.fn().mockReturnThis(),
        select: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        orderBy: jest.fn().mockReturnThis(),
        setParameters: jest.fn().mockReturnThis(),
        getRawMany: jest.fn().mockResolvedValue(mockStationData),
      });

      await StationController.prototype.getByLocation(
        req,
        mockResponse as Response,
        next,
      );

      expect(mockResponse.status).not.toHaveBeenCalled();
      expect(mockResponse.json).toHaveBeenCalledWith({
        "40.1:-74.1": mockStationData,
      });
    });
  });
});
