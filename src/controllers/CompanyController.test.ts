import { Request, Response } from "express";
import { CompanyController } from "./CompanyController";
import { dataSource } from "../app-data-source";

jest.mock("../app-data-source", () => ({
  dataSource: {
    getRepository: jest.fn(),
  },
  initializeDataSource: jest.fn(),
}));

describe("CompanyController", () => {
  let mockResponse: Partial<Response>;

  beforeEach(() => {
    mockResponse = {
      json: jest.fn(),
      status: jest.fn().mockReturnThis(),
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should get all companies", async () => {
    const mockCompanies = [
      { id: 1, name: "Company 1" },
      { id: 2, name: "Company 2" },
    ];
    dataSource.getRepository = jest.fn().mockReturnValue({
      findAndCount: jest.fn().mockResolvedValue(mockCompanies),
    });

    const mockRequest = { query: { page: 1, pageSize: 2 } } as any as Request;
    await CompanyController.prototype.getAll(
      mockRequest,
      mockResponse as Response,
    );
    expect(mockResponse.json).toHaveBeenCalledWith({
      rows: mockCompanies[0],
      count: mockCompanies[1],
    });
  });

  it("should get a company by id", async () => {
    const mockCompany = { id: 1, name: "Company 1" };
    dataSource.getRepository = jest.fn().mockReturnValue({
      findOne: jest.fn().mockResolvedValue(mockCompany),
    });

    const mockRequest = { params: { id: "1" } } as Request<any>;
    await CompanyController.prototype.getById(
      mockRequest,
      mockResponse as Response,
    );

    expect(mockResponse.json).toHaveBeenCalledWith(mockCompany);
  });

  it("should create a company", async () => {
    const mockCompany = { id: 1, name: "New Company" };
    dataSource.getRepository = jest.fn().mockReturnValue({
      create: jest.fn().mockReturnValue(mockCompany),
      save: jest.fn().mockResolvedValue(mockCompany),
    });

    const mockRequest = { body: { name: "New Company" } } as Request;
    await CompanyController.prototype.create(
      mockRequest,
      mockResponse as Response,
      jest.fn(),
    );

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.json).toHaveBeenCalledWith(mockCompany);
  });

  it("should update a company", async () => {
    const mockCompany = { id: 1, name: "Company 1" };
    const mockUpdatedCompany = {
      ...mockCompany,
      name: "Updated Company",
    };
    dataSource.getRepository = jest.fn().mockReturnValue({
      findOne: jest.fn().mockResolvedValue(mockCompany),
      merge: jest.fn().mockReturnThis(),
      save: jest.fn().mockResolvedValue(mockUpdatedCompany),
    });

    const mockRequest = {
      params: { id: "1" },
      body: { name: "Updated Company" },
    } as Request<any>;
    await CompanyController.prototype.update(
      mockRequest,
      mockResponse as Response,
      jest.fn(),
    );

    expect(mockResponse.json).toHaveBeenCalledWith(mockUpdatedCompany);
  });

  it("should delete a company", async () => {
    dataSource.getRepository = jest.fn().mockReturnValue({
      delete: jest.fn().mockResolvedValue({ affected: 1 }),
    });

    const mockRequest = { params: { id: "1" } } as Request<any>;
    await CompanyController.prototype.delete(
      mockRequest,
      mockResponse as Response,
    );

    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "Company deleted successfully",
    });
  });
});
