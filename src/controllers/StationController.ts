import { Request, Response, NextFunction } from "express";
import { Station } from "../entitiy/Station";
import { dataSource } from "../app-data-source";

export class StationController {
  async getAll(req: Request, res: Response) {
    const page = parseInt(req.query.page as string, 10) || 1;
    const pageSize = parseInt(req.query.pageSize as string, 10) || 10;

    const offset = (page - 1) * pageSize;

    const result = await dataSource.getRepository(Station).findAndCount({
      relations: {
        company: true,
      },
      take: pageSize,
      skip: offset,
      order: {
        id: "ASC",
      },
    });

    return res.json({ rows: result[0], count: result[1] });
  }

  async getById(req: Request, res: Response) {
    if (!req.params.id) {
      return res.status(400).json({ message: "Missing resource id" });
    }

    const station = await dataSource.getRepository(Station).findOne({
      where: { id: Number(req.params.id) },
      relations: {
        company: true,
      },
    });

    if (!station) {
      return res.status(404).json({ message: "Station not found" });
    }

    return res.json(station);
  }

  async getByLocation(req: Request, res: Response, next: NextFunction) {
    if (!req.params.latitude || !req.params.longitude) {
      return res
        .status(400)
        .json({ message: "Missing latitude and/or longitude" });
    }

    try {
      const range = Number(req.params.range) || 1000; // default 1000 meters

      const origin = {
        type: "Point",
        coordinates: [req.params.longitude, req.params.latitude],
      };

      const stations = await dataSource
        .getRepository(Station)
        .createQueryBuilder()
        .select([
          "Station.id as id",
          "Station.name as name",
          "Station.address as address",
          "Station.latitude as latitude",
          "Station.longitude as longitude",
          "Station.company as company",
          "ST_Distance(location, ST_SetSRID(ST_GeomFromGeoJSON(:origin), ST_SRID(location)))/1000 AS distance",
        ])
        .where(
          "ST_DWithin(location, ST_SetSRID(ST_GeomFromGeoJSON(:origin), ST_SRID(location)) ,:range)",
        )
        .orderBy("distance", "ASC")
        .setParameters({
          origin: JSON.stringify(origin),
          range: range * 1000, // KM conversion
        })
        .getRawMany();

      if (stations.length) {
        // group stations with same location
        const groupedStations = stations.reduce((acc, station) => {
          const key = `${station.latitude}:${station.longitude}`;
          if (!acc[key]) {
            acc[key] = [];
          }
          acc[key].push(station);
          return acc;
        }, {});

        return res.json(groupedStations);
      }

      return res.status(404).json({ message: "Stations not found" });
    } catch (err) {
      next(err);
    }
  }

  async create(req: Request, res: Response, next: NextFunction) {
    const { name, latitude, longitude, address, company } = req.body;

    if (!name || !latitude || !longitude || !address || !company) {
      return res.status(400).json({ message: "Missing mandatory fields" });
    }

    try {
      const station = dataSource.getRepository(Station).create({
        name,
        address,
        longitude,
        latitude,
        location: { type: "Point", coordinates: [latitude, longitude] },
        company,
      });

      const savedStation = await dataSource
        .getRepository(Station)
        .save(station);

      return res.status(201).json(savedStation);
    } catch (err) {
      next(err);
    }
  }

  async update(req: Request, res: Response, next: NextFunction) {
    if (!req.params.id) {
      return res.status(400).json({ message: "Missing resource id" });
    }

    const { name, latitude, longitude, address, company } = req.body;

    if (!name && !latitude && !longitude && !address && !company) {
      return res
        .status(400)
        .json({ message: "Please provide at least one field to update" });
    }

    try {
      const station = await dataSource
        .getRepository(Station)
        .findOne({ where: { id: Number(req.params.id) } });

      if (!station) {
        return res.status(404).json({ message: "Station not found" });
      }

      dataSource.getRepository(Station).merge(station, {
        name,
        latitude,
        longitude,
        address,
        company,
        location:
          latitude && longitude
            ? { type: "Point", coordinates: [latitude, longitude] }
            : undefined,
      });
      const updatedCompany = await dataSource
        .getRepository(Station)
        .save(station);

      return res.json(updatedCompany);
    } catch (err) {
      next(err);
    }
  }

  async delete(req: Request, res: Response) {
    await dataSource.getRepository(Station).delete(req.params.id);

    return res.json({ message: "Station deleted successfully" });
  }
}
