import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  Point,
} from "typeorm";
import { Company } from "./Company";

@Entity()
export class Station {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  latitude: number;

  @Column()
  longitude: number;

  @Column()
  address: string;

  @Column({
    type: "geometry",
    spatialFeatureType: "Point",
    srid: 4326, // WGS 84 coordinate system
  })
  location: Point;

  @ManyToOne(() => Company, (company) => company.stations)
  company: Company;
}
