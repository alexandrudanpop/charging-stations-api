import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  OneToOne,
  JoinColumn,
} from "typeorm";
import { Station } from "./Station";

@Entity()
export class Company {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToOne(() => Company)
  @JoinColumn()
  parentCompany?: Company;

  @OneToMany(() => Station, (station) => station.company)
  stations: Station[];
}
