import { DataSource } from "typeorm";
import { Company } from "./entitiy/Company";
import { Station } from "./entitiy/Station";

function retryWithExponentialBackOff(
  promise: () => Promise<any>,
  maxRetries = 5,
  delay = 1000,
) {
  return new Promise((resolve, reject) => {
    promise()
      .then(resolve)
      .catch((error) => {
        if (maxRetries === 0) {
          return reject(error);
        }

        setTimeout(() => {
          retryWithExponentialBackOff(promise, maxRetries - 1, delay * 2).then(
            resolve,
            reject,
          );
        }, delay);
      });
  });
}

export const dataSource = new DataSource({
  type: "postgres",
  host: process.env.POSTGRES_HOST || "localhost",
  port: Number(process.env.POSTGRES_PORT) || 5433,
  username: process.env.POSTGRES_USERNAME || "postgresUser",
  password: process.env.POSTGRES_PASSWORD || "postgresPW",
  database: process.env.POSTGRES_DB || "postgresDB",
  entities: [Company, Station],
  logging: true,
  synchronize: true,
});

retryWithExponentialBackOff(() =>
  dataSource
    .initialize()
    .then(() => {
      console.log("Data Source has been initialized!");
    })
    .catch((err) => {
      console.error("Error during Data Source initialization:", err);
    }),
);
